/** Nine different interpreters for Jam that differ in binding
 *  policy and cons evaluation policy.
 * 
 *  Binding Policy is either:
 *  call-by-value,
 *  call-by-name, or
 *  call-by-need.
 * 
 *  The cons evaluation policy is either:
 *  call-by-value (eager),
 *  call-by-name (redundant lazy), or
 *  call-by-need (efficient lazy).
 */

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/** Interpreter Classes */

/**
 * The exception class for Jam run-time errors.
 */
class EvalException extends RuntimeException {

	private static final long serialVersionUID = 1962023094252778366L;

	/**
	 * Constructor for this exception.
	 * @param msg description
	 */
	EvalException(String msg) {
		super(msg);
	}
}

/**
 * The visitor interface for interpreting ASTs.
 */
interface EvalVisitor extends ASTVisitor<JamVal> {
	/**
	 * Constructs a new visitor of same form with specified environment e.
	 * @param e environment
	 * @return evaluation visitor with the specified environment
	 */
	EvalVisitor newEvalVisitor(PureList<Binding> e);

	/**
	 * Returns the environment embedded in this EvalVisitor.
	 * @return environment
	 */
	PureList<Binding> env();
}

/**
 * The interface supported by various binding evaluation policies: call-by-value, call-by-name, and call-by-need.
 */
interface BindingPolicy {
	/**
	 * Constructs the appropriate binding object for this, binding var to ast in the evaluator ev.
	 * @param var variable
	 * @param ast AST
	 * @param ev evaluation visitor
	 * @return new binding
	 */
	Binding newBinding(Variable var, AST ast, EvalVisitor ev);

	/**
	 * Constructs the appropriate dummy binding object for this.
	 * @param var variable
	 * @return new dummy binding
	 */
	Binding newDummyBinding(Variable var);
}

/**
 * Interface for various cons evaluation policies. A ConsPolicy is typically embedded inside an BindingPolicy.
 */
interface ConsPolicy {
	/**
	 * Constructs the appropriate cons.
	 * @param args arguments
	 * @param ev evaluation visitor
	 * @return cons
	 */
	JamVal evalCons(AST[] args, EvalVisitor ev);
}

/**
 * Interface for classes with a variable field (Variable and the various Binding classes).
 */
interface WithVariable {
	/**
	 * Accessor for the variable.
	 * @return variable
	 */
	Variable var();
}

/**
 * A lookup visitor class that returns element matching the embedded var. If no match found, returns null.
 */
class LookupVisitor<ElemType extends WithVariable> implements PureListVisitor<ElemType, ElemType> {
	/**
	 * Variable to look up.
	 */
	Variable var;

	// the lexer guarantees that there is only one Variable for a given name

	/**
	 * Constructor for a look-up visitor.
	 * @param v variable to look up
	 */
	LookupVisitor(Variable v) {
		var = v;
	}

	/**
	 * Case for empty lists.
	 * @param e host
	 * @return always null
	 */
	public ElemType forEmpty(Empty<ElemType> e) {
		return null;
	}

	/**
	 * Case for non-empty lists.
	 * @param c host
	 * @return the element of the list if it is found, otherwise null
	 */
	public ElemType forCons(Cons<ElemType> c) {
		// System.err.println("forCons in LookUpVisitor invoked; c = " + c);
		ElemType e = c.first();
		if (var.equals(e.var())) {
			return e;
		}
		return c.rest().accept(this);
	}
}

/**
 * Interpreter class.
 */
class Interpreter {
	/**
	 * Parser to use.
	 */
	Parser parser = null;

	/**
	 * Parsed AST.
	 */
	AST prog = null;

	/**
	 * Constructor for the interpreter.
	 * @param fileName file name
	 * @throws IOException
	 */
	Interpreter(String fileName) throws IOException {
		parser = new Parser(fileName);
		prog = parser.parseAndCheck();
	}

	/**
	 * Constructor for the interpreter.
	 * @param p parser
	 */
	Interpreter(Parser p) {
		parser = p;
		prog = parser.parseAndCheck();
	}

	/**
	 * Constructor for the interpreter.
	 * @param reader reader with input
	 */
	Interpreter(Reader reader) {
		parser = new Parser(reader);
		prog = parser.parseAndCheck();
	}

	/**
	 * Parses and VV interprets the input embeded in parser.
	 * @return result of evaluation
	 */
	public JamVal eval() {
		return prog.accept(valueValueVisitor);
	}

	/**
	 * Performs variable unshadowing on the program
	 */
	public AST unshadow(){
		return prog.accept(new UnshadowingVisitor(0,new Empty<Variable>()));
	}

	/**
	 * Performs a cps transformation on the program
	 */
	public AST convertToCPS(){
		Variable x = new Variable("x");
		Variable[] xArg = {x};
		Map id = new Map(xArg,x);
		AST unshadowed = unshadow();
		return unshadowed.accept(new CPSVisitor(id));
	}

	/**
	 * Evaluates a CPS-transformed version of the program
	 */
	public JamVal cpsEval(){
		AST cps = convertToCPS();
		return cps.accept(valueValueVisitor);
	}

	/**
	 * Perform static distance transformation on the program
	 */
	public AST convertToSD(){
		return prog.accept(StaticDistanceVisitor.INITIAL);
	}

	/**
	 * Evaluates a sd-transformed program
	 */
	public JamVal SDEval(){
		return convertToSD().accept(sdVisitor);
	}

	/**
	 * Evaluates a CPSed sd-transformed program
	 * @return
	 */
	public JamVal SDCpsEval(){
		return convertToCPS().accept(StaticDistanceVisitor.INITIAL).accept(sdVisitor);
	}

	/**
	 * Binding policy for call-by-value.
	 */
	static final BindingPolicy CALL_BY_VALUE = new BindingPolicy() {
		public Binding newBinding(Variable var, AST arg, EvalVisitor ev) {
			return new ValueBinding(var, arg.accept(ev));
		}
		public Binding newDummyBinding(Variable var) {
			return new ValueBinding(var, null);
		}
	};

	/**
	 * Binding policy for call-by-name.
	 */
	static final BindingPolicy CALL_BY_NAME = new BindingPolicy() {
		public Binding newBinding(Variable var, AST arg, EvalVisitor ev) {
			return new NameBinding(var, new Suspension(arg, ev));
		}
		public Binding newDummyBinding(Variable var) {
			return new NameBinding(var, null);
		}
	};

	/**
	 * Binding policy for call-by-need.
	 */
	static final BindingPolicy CALL_BY_NEED = new BindingPolicy() {
		public Binding newBinding(Variable var, AST arg, EvalVisitor ev) {
			return new NeedBinding(var, new Suspension(arg, ev));
		}

		public Binding newDummyBinding(Variable var) {
			return new NeedBinding(var, null);
		}
	};

	/**
	 * Eager cons evaluation policy. presume that args has exactly 2 elements.
	 */
	public static final ConsPolicy EAGER = new ConsPolicy() {
		public JamVal evalCons(AST[] args, EvalVisitor ev) {
			JamVal val0 = args[0].accept(ev);
			JamVal val1 = args[1].accept(ev);
			if (val1 instanceof JamList) {
				return new JamCons(val0, (JamList)val1);
			}
			throw new EvalException("Second argument " + val1 + " to `cons' is not a JamList");
		}
	};

	/**
	 * Call-by-name lazy cons evaluation policy.
	 */
	public static final ConsPolicy LAZYNAME = new ConsPolicy() {
		public JamVal evalCons(AST[] args, EvalVisitor ev) {
			/* System.out.println("LazyNameCons called on " + ToString.toString(args, ", ")); */
			return new JamLazyNameCons(args[0], args[1], ev);
		}
	};

	/**
	 * Call-by-need lazy cons evaluation policy.
	 */
	public static final ConsPolicy LAZYNEED = new ConsPolicy() {
		public JamVal evalCons(AST[] args, EvalVisitor ev) {
			/* System.out.println("LazyNeedCons called on " + ToString.toString(args, ", ")); */
			return new JamLazyNeedCons(args[0], args[1], ev);
		}
	};

	/**
	 * Value-value visitor.
	 */
	static final ASTVisitor<JamVal> valueValueVisitor = new Evaluator(CALL_BY_VALUE, EAGER);

	/**
	 * Value-name visitor.
	 */
	static final ASTVisitor<JamVal> valueNameVisitor = new Evaluator(CALL_BY_VALUE, LAZYNAME);

	/**
	 * Value-need visitor.
	 */
	static final ASTVisitor<JamVal> valueNeedVisitor = new Evaluator(CALL_BY_VALUE, LAZYNEED);

	/**
	 * Name-value visitor.
	 */
	static final ASTVisitor<JamVal> nameValueVisitor = new Evaluator(CALL_BY_NAME, EAGER);

	/**
	 * Name-name visitor.
	 */
	static final ASTVisitor<JamVal> nameNameVisitor = new Evaluator(CALL_BY_NAME, LAZYNAME);

	/**
	 * Name-need visitor.
	 */
	static final ASTVisitor<JamVal> nameNeedVisitor = new Evaluator(CALL_BY_NAME, LAZYNEED);

	/**
	 * Need-value visitor.
	 */
	static final ASTVisitor<JamVal> needValueVisitor = new Evaluator(CALL_BY_NEED, EAGER);

	/**
	 * Need-name visitor.
	 */
	static final ASTVisitor<JamVal> needNameVisitor = new Evaluator(CALL_BY_NEED, LAZYNAME);

	/**
	 * Need-need visitor.
	 */
	static final ASTVisitor<JamVal> needNeedVisitor = new Evaluator(CALL_BY_NEED, LAZYNEED);

	/**
	 * Static distance visitor
	 */
	static final ASTVisitor<JamVal> sdVisitor = new SDEvaluator();

	/**
	 * JVM entry point. Interpret the specified file.
	 * @param args command line arguments
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String input = "letrec append :=       map x,y to          if x = null then y else cons(first(x), append(rest(x), y));  in let  l      := cons(1,cons(2,cons(3,null))); in append(l,l)";
		Interpreter i = new Interpreter(new StringReader(input));
		System.out.println(i.unshadow());
		System.out.println(i.convertToCPS());
		System.out.println(i.convertToSD());
		System.out.println(i.convertToCPS().accept(StaticDistanceVisitor.INITIAL));
		System.out.println(i.eval());
		System.out.println(i.cpsEval());
		System.out.println(i.SDEval());
		System.out.println(i.SDCpsEval());

		/**
		System.out.println(i.convertToSD());
		System.out.println(i.convertToCPS().accept(StaticDistanceVisitor.INITIAL));
		System.out.println(i.eval());
		System.out.println(i.sdEval());
		System.out.println(i.sdCpsEval());*/
		/**
        Parser p;
        if (args.length == 0) {
            System.out.println("Usage: java Interpreter <filename> { value | name | need }");
            return;
        }
        p = new Parser(args[0]);
        AST prog = p.parse();
        System.out.println("Parse tree is: " + prog);
		 */
	}
}

/**
 * Primary visitor class for performing interpretation
 */
class Evaluator implements EvalVisitor {

	// Assumes that:
	//   OpTokens are unique
	//   Variable objects are unique: v1.name.equals(v.name) => v1 == v2
	//   only objects used as boolean values are BoolConstant.TRUE and BoolConstant.FALSE

	// Hence,  == can be used to compare Variable objects, OpTokens, and
	// BoolConstants

	/**
	 * Environment.
	 */
	PureList<Binding> env;

	/**
	 * Policy to create bindings.
	 */
	BindingPolicy bindingPolicy;

	/**
	 * Policy to create cons.
	 */
	ConsPolicy consPolicy;

	/**
	 * Constructor for this evaluation visitor.
	 * @param e environment
	 * @param bp binding policy
	 * @param cp cons policy
	 */
	private Evaluator(PureList<Binding> e, BindingPolicy bp, ConsPolicy cp) {
		env = e;
		bindingPolicy = bp;
		consPolicy = cp;
	}

	/**
	 * Constructor for this evaluation visitor with an empty environment.
	 * @param bp binding policy
	 * @param cp cons policy
	 */
	public Evaluator(BindingPolicy bp, ConsPolicy cp) {
		this(new Empty<Binding>(), bp, cp);
	}

	/* EvalVisitor methods */

	/**
	 * Factory method that constructs a visitor similar to this with environment env
	 * @param env environment for the new visitor.
	 * @return new visitor with the same policies
	 */
	public EvalVisitor newEvalVisitor(PureList<Binding> env) {
		return new Evaluator(env, bindingPolicy, consPolicy);
	}

	/**
	 * Getter for env field.
	 * @return environment
	 */
	public PureList<Binding> env() {
		return env;
	}

	/**
	 * Case for BoolConstants.
	 * @param b host
	 * @return host
	 */
	public JamVal forBoolConstant(BoolConstant b) {
		return b;
	}

	/**
	 * Case for IntConstants.
	 * @param i host
	 * @return host
	 */
	public JamVal forIntConstant(IntConstant i) {
		return i;
	}

	/**
	 * Case for NullConstants.
	 * @param n host
	 * @return empty Jam list
	 */
	public JamVal forNullConstant(NullConstant n) {
		return JamEmpty.ONLY;
	}

	/**
	 * Case for Variables.
	 * @param v host
	 * @return value of variable in current environment
	 */
	public JamVal forVariable(Variable v) {
		Binding match = env.accept(new LookupVisitor<Binding>(v));
		if (match == null) {
			throw new EvalException("variable " + v + " is unbound");
		}
		return match.value();
	}

	/**
	 * Case for PrimFuns.
	 * @param f host
	 * @return host
	 */
	public JamVal forPrimFun(PrimFun f) {
		return f;
	}

	/**
	 * Case for UnOpApps.
	 * @param u host
	 * @return value of unary operator application
	 */
	public JamVal forUnOpApp(UnOpApp u) {
		return u.rator().accept(new UnOpEvaluator(u.arg().accept(this)));
	}

	/**
	 * Case for BinOpApps.
	 * @param b host
	 * @return value of unary operator application
	 */
	public JamVal forBinOpApp(BinOpApp b) {
		return b.rator().accept(new BinOpEvaluator(b.arg1(), b.arg2()));
	}

	/**
	 * Case for Apps.
	 * @param a host
	 * @return value of application
	 */
	public JamVal forApp(App a) {
		JamVal rator = a.rator().accept(this);
		if (rator instanceof JamFun) {
			return ((JamFun)rator).accept(new FunEvaluator(a.args()));
		}
		throw new EvalException(rator + " appears at head of application " + a
				+ " but it is not a valid function");
	}

	/**
	 * Case for Maps.
	 * @param m host
	 * @return closure
	 */
	public JamVal forMap(Map m) {
		return new JamClosure(m, env);
	}

	/**
	 * Case for Ifs.
	 * @param i host
	 * @return value of conditional expression
	 */
	public JamVal forIf(If i) {
		JamVal test = i.test().accept(this);
		if (!(test instanceof BoolConstant)) {
			throw new EvalException("non Boolean " + test + " used as test in if");
		}
		if (test == BoolConstant.TRUE) {
			return i.conseq().accept(this);
		}
		return i.alt().accept(this);
	}

	/**
	 * Case for LetRecs.
	 * @param l host
	 * @return value of body
	 */
	public JamVal forLetRec(LetRec l) {
		/* recursive let semantics */

		/* Extract binding vars and exps (rhs's) from l */
		Variable[] vars = l.vars();
		AST[] exps = l.exps();
		int n = vars.length;

		// construct newEnv for Let body and exps; vars are bound to values of corresponding exps using newEvalVisitor
		PureList<Binding> newEnv = env();
		Binding[] bindings = new Binding[n];
		for(int i = n - 1; i >= 0; i--) {
			bindings[i] = bindingPolicy.newDummyBinding(vars[i]);  // bind var[i] to dummy value null
			newEnv = newEnv.cons(bindings[i]);          // add new Binding to newEnv; it is shared!
		}
		EvalVisitor newEV = newEvalVisitor(newEnv);

		// fix up the dummy values
		for(int i = 0; i < n; i++) {
			bindings[i].setBinding(exps[i], newEV);  // modifies newEnv and newEvalVisitor
		}
		return l.body().accept(newEV);
	}

	/**
	 * Case for Lets.
	 * @param l host
	 * @return value of body
	 */
	public JamVal forLet(Let l) {
		/* non-recursive let semantics */

		/* Extract binding vars and exps (rhs's) from l */
		Variable[] vars = l.vars();
		AST[] exps = l.exps();
		int n = vars.length;

		// construct newEnv for Let body; vars are bound to values of corresponding exps using this visitor, not the new environment
		PureList<Binding> newEnv = env();
		Binding[] bindings = new Binding[n];
		for(int i = n - 1; i >= 0; i--) {
			bindings[i] = bindingPolicy.newBinding(vars[i], exps[i], this);
			newEnv = newEnv.cons(bindings[i]);          // add new Binding to newEnv; it is shared!
		}
		EvalVisitor newEV = newEvalVisitor(newEnv);

		// use newEnv for body only
		return l.body().accept(newEV);
	}

	/* Inner classes */

	/**
	 * Function evaluator.
	 */
	class FunEvaluator implements FunVisitor<JamVal> {
		/**
		 * Unevaluated arguments
		 */
		AST[] args;

		/**
		 * Constructor for the function evaluator.
		 * @param asts unevaluated arguments
		 */
		FunEvaluator(AST[] asts) {
			args = asts;
		}

		/* Support for FunVisitor<JamVal> interface */

		/**
		 * Case for Jam closures.
		 * @param closure closure
		 * @return result of application
		 */
		public JamVal forJamClosure(JamClosure closure) {
			Map map = closure.body();
			int n = args.length;
			Variable[] vars = map.vars();
			if (vars.length != n) {
				throw new EvalException("closure " + closure + " applied to " + n +
						" arguments");
			}

			// construct newEnv for JamClosure body using JamClosure env
			PureList<Binding> newEnv = closure.env();
			for(int i = n - 1; i >= 0; i--) {
				newEnv = newEnv.cons(bindingPolicy.newBinding(vars[i], args[i],Evaluator.this));
			}
			return map.body().accept(newEvalVisitor(newEnv));
		}

		/**
		 * Case for PrimFuns.
		 * @param primFun host
		 * @return result of application
		 */
		public JamVal forPrimFun(PrimFun primFun) {
			return primFun.accept(primEvaluator);
		}

		/**
		 * Evaluator for PrimFuns.
		 */
		PrimFunVisitor<JamVal> primEvaluator = new PrimFunVisitor<JamVal>() {
			/**
			 * Evaluate args using evaluation visitor in whose closure this object is.
			 * @return array of evaluated arguments.
			 */
			private JamVal[] evalArgs() {
				int n = args.length;
				JamVal[] vals = new JamVal[n];
				for(int i = 0; i < n; i++) {
					vals[i] = args[i].accept(Evaluator.this);
				}
				return vals;
			}

			/**
			 * Throw an error.
			 */
			private void primFunError(String fn) {
				throw new EvalException("Primitive function `" + fn + "' applied to " +
						args.length + " arguments");
			}

			/**
			 * Evaluate an argument that has to be a Jam cons.
			 * @param arg argument
			 * @param fun function performing this evaluation
			 * @return Jam cons
			 */
			private JamCons evalJamConsArg(AST arg, String fun) {
				JamVal val = arg.accept(Evaluator.this);
				if (val instanceof JamCons) {
					return (JamCons)val;
				}
				throw new EvalException("Primitive function `" + fun + "' applied to argument " + val +
						" that is not a JamCons");
			}

			/**
			 * Case for function?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamFun
			 */
			public JamVal forFunctionPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("function?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamFun);
			}

			/**
			 * Case for number?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a IntConstant
			 */
			public JamVal forNumberPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("number?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof IntConstant);
			}

			/**
			 * Case for list?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamList
			 */
			public JamVal forListPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("list?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamList);
			}

			/**
			 * Case for cons?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamCons
			 */
			public JamVal forConsPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("cons?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamCons);
			}

			/**
			 * Case for null?
			 *
			 * @return BoolConstant.TRUE if argument evaluates to a JamEmpty
			 */
			public JamVal forNullPPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("null?");
				}
				return BoolConstant.toBoolConstant(vals[0] instanceof JamEmpty);
			}

			/**
			 * Case for cons
			 *
			 * @return JamCons
			 */
			public JamVal forConsPrim() {
				if (args.length != 2) {
					primFunError("cons");
				}
				return consPolicy.evalCons(args, Evaluator.this);  /* Evaluation strategy determined by consEp */
			}

			/**
			 * Case for arity
			 * @return IntConstant representing the arity of the argument
			 */
			public JamVal forArityPrim() {
				JamVal[] vals = evalArgs();
				if (vals.length != 1) {
					primFunError("arity");
				}
				if (!(vals[0] instanceof JamFun)) {
					throw new EvalException("arity applied to argument " +
							vals[0]);
				}
				return ((JamFun)vals[0]).accept(new FunVisitor<IntConstant>() {
					public IntConstant forJamClosure(JamClosure jc) {
						return new IntConstant(jc.body().vars().length);
					}

					public IntConstant forPrimFun(PrimFun jpf) {
						return new IntConstant(jpf.accept(ArityVisitor.ONLY));
					}
				});
			}

			/**
			 * Case for first
			 * @return first
			 */
			public JamVal forFirstPrim() {
				if (args.length != 1) {
					primFunError("first");
				}
				return evalJamConsArg(args[0], "first").first();
			}

			/**
			 * Case for rest
			 * @return rest
			 */
			public JamVal forRestPrim() {
				if (args.length != 1) {
					primFunError("rest");
				}
				return evalJamConsArg(args[0], "rest").rest();
			}


			public JamVal forRefPPrim() {
				if (args.length != 1) {
					primFunError("ref");
				}
				return BoolConstant.toBoolConstant(args[0].accept(Evaluator.this) instanceof JamBox);
			}
		};
	}

	/**
	 * Evaluator for unary operators.
	 */
	static class UnOpEvaluator implements UnOpVisitor<JamVal> {
		/**
		 * Value of the operand.
		 */
		private JamVal val;

		/**
		 * Constructor for this evaluator.
		 * @param jv value of the operand
		 */
		UnOpEvaluator(JamVal jv) {
			val = jv;
		}

		/**
		 * Return the value of the operand if it is an IntConstant, otherwise throw an exception,
		 * @param op operator the value gets applied to
		 * @return value of the operand, if it is an IntConstant
		 */
		private IntConstant checkInteger(UnOp op) {
			if (val instanceof IntConstant) {
				return (IntConstant)val;
			}
			throw new EvalException("Unary operator `" + op + "' applied to non-integer " + val);
		}

		/**
		 * Return the value of the operand if it is a BoolConstant, otherwise throw an exception,
		 * @param op operator the value gets applied to
		 * @return value of the operand, if it is an BoolConstant
		 */
		private BoolConstant checkBoolean(UnOp op) {
			if (val instanceof BoolConstant) {
				return (BoolConstant)val;
			}
			throw new EvalException("Unary operator `" + op + "' applied to non-boolean " + val);
		}

		private JamBox checkBox(UnOp op){
			if (val instanceof JamBox){
				return (JamBox)val;
			}
			throw new EvalException("Unary operator '" + op + "' applied to non-reference value " + val);
		}

		/**
		 * Case for unary plus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forUnOpPlus(UnOpPlus op) {
			return checkInteger(op);
		}

		/**
		 * Case for unary minus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forUnOpMinus(UnOpMinus op) {
			return new IntConstant(-checkInteger(op).value());
		}

		/**
		 * Case for not.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpTilde(OpTilde op) {
			return checkBoolean(op).not();
		}

		@Override
		public JamVal forOpBang(OpBang op) {
			return checkBox(op).value();
		}

		@Override
		public JamVal forOpRef(OpRef op) {
			return new JamBox(val);
		}
	}

	/**
	 * Evaluator for binary operators.
	 */
	class BinOpEvaluator implements BinOpVisitor<JamVal> {
		/**
		 * Unevaluated arguments.
		 */
		private AST arg1, arg2;

		/**
		 * Constructor for this evaluator
		 * @param a1 unevaluted first argument
		 * @param a2 unevaluated second argument
		 */
		BinOpEvaluator(AST a1, AST a2) {
			arg1 = a1;
			arg2 = a2;
		}

		/**
		 * Return the value of the argument if it is an IntConstant, otherwise throw an exception,
		 * @param arg argument to evaluate and check
		 * @param b operator the value gets applied to
		 * @return value of the argument, if it is an IntConstant
		 */
		private IntConstant evalIntegerArg(AST arg, BinOp b) {
			JamVal val = arg.accept(Evaluator.this);
			if (val instanceof IntConstant) {
				return (IntConstant)val;
			}
			throw new EvalException("Binary operator `" + b + "' applied to non-integer " + val);
		}

		/**
		 * Return the value of the argument if it is a BoolConstant, otherwise throw an exception,
		 * @param arg argument to evaluate and check
		 * @param b operator the value gets applied to
		 * @return value of the argument, if it is a BoolConstant
		 */
		private BoolConstant evalBooleanArg(AST arg, BinOp b) {
			JamVal val = arg.accept(Evaluator.this);
			if (val instanceof BoolConstant) {
				return (BoolConstant)val;
			}
			throw new EvalException("Binary operator `" + b + "' applied to non-boolean " + val);
		}

		private JamBox evalJamBoxArg(AST arg, BinOp b){
			JamVal val = arg.accept(Evaluator.this);
			if (val instanceof JamBox){
				return (JamBox)val;
			}
			throw new EvalException("Binary operator `" + b + "' applied to non-reference value " + val);
		}

		/**
		 * Case for binary plus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forBinOpPlus(BinOpPlus op) {
			return new IntConstant(evalIntegerArg(arg1, op).value() + evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for binary minus.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forBinOpMinus(BinOpMinus op) {
			return new IntConstant(evalIntegerArg(arg1, op).value() - evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for times.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpTimes(OpTimes op) {
			return new IntConstant(evalIntegerArg(arg1, op).value() * evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for divide.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpDivide(OpDivide op) {
			int divisor = evalIntegerArg(arg2, op).value();
			if (divisor == 0) {
				throw new EvalException("Attempt to divide by zero");
			}
			return new IntConstant(evalIntegerArg(arg1, op).value() / divisor);
		}

		/**
		 * Case for equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpEquals(OpEquals op) {
			return BoolConstant.toBoolConstant(arg1.accept(Evaluator.this).equals(arg2.accept(Evaluator.this)));
		}

		/**
		 * Case for not equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpNotEquals(OpNotEquals op) {
			return BoolConstant.toBoolConstant(!arg1.accept(Evaluator.this).equals(arg2.accept(Evaluator.this)));
		}

		/**
		 * Case for less than.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpLessThan(OpLessThan op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() < evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for greater than.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpGreaterThan(OpGreaterThan op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() > evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for less than or equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpLessThanEquals(OpLessThanEquals op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() <= evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for greater than or equals.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpGreaterThanEquals(OpGreaterThanEquals op) {
			return BoolConstant.toBoolConstant(evalIntegerArg(arg1, op).value() >= evalIntegerArg(arg2, op).value());
		}

		/**
		 * Case for and.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpAnd(OpAnd op) {
			BoolConstant b1 = evalBooleanArg(arg1, op);
			if (b1 == BoolConstant.FALSE) {
				return BoolConstant.FALSE;
			}
			return evalBooleanArg(arg2, op);
		}

		/**
		 * Case for or.
		 * @param op host
		 * @return value of the operator application
		 */
		public JamVal forOpOr(OpOr op) {
			BoolConstant b1 = evalBooleanArg(arg1, op);
			if (b1 == BoolConstant.TRUE) {
				return BoolConstant.TRUE;
			}
			return evalBooleanArg(arg2, op);
		}

		@Override
		public JamVal forOpGets(OpGets op) {
			JamBox v1 = evalJamBoxArg(arg1,op);
			JamVal v2 = arg2.accept(Evaluator.this);
			v1.mutate(v2);
			return JamVoid.ONLY;
		}
	}

	@Override
	public JamVal forBlock(Block b) {
		JamVal last = null;
		for(AST exp : b.exps){
			last = exp.accept(this);
		}
		return last;
	}
}

/**
 * An evaluator for programs converted into a static-distance format
 *
 */
class SDEvaluator extends Evaluator{
	PureList<JamVal[]> sdEnv;

	public SDEvaluator(){
		super(Interpreter.CALL_BY_VALUE, Interpreter.EAGER);
		sdEnv = new Empty<JamVal[]>();
	}

	public SDEvaluator(PureList<JamVal[]> newEnv){
		super(Interpreter.CALL_BY_VALUE, Interpreter.EAGER);
		sdEnv = newEnv;
	}

	public JamVal forVariable(Variable v){
		final int[] depth = {0};
		final VariableSD vsd = (VariableSD)v;
		JamVal val = sdEnv.accept(new PureListVisitor<JamVal[],JamVal>(){
			public JamVal forEmpty(Empty<JamVal[]> e) {
				return null;
			}

			public JamVal forCons(Cons<JamVal[]> c) {
				if(vsd.sdDepth() == depth[0]){
					JamVal[] vals = c.first();
					if(vsd.sdIndex() < vals.length){
						return vals[vsd.sdIndex()];
					}
					else{
						return null;
					}
				}
				else{
					depth[0] += 1;
					return c.rest().accept(this);
				}
			}

		});
		if(val != null){
			return val;
		}
		else{
			throw new EvalException(v + " is not a valid variable");
		}
	}

	public JamVal forLet(Let l){
		PureList<JamVal[]> newEnv = sdEnv;
		newEnv = newEnv.cons(evalAll(l.exps()));
		return l.body().accept(new SDEvaluator(newEnv));
	}

	public JamVal forLetRec(LetRec l){
		//Create a dummy frame, evaluate the rhs, and replace the
		//dummy frame with the evaluated frame
		AST[] args = l.exps();
		JamVal[] frame = new JamVal[args.length];
		JamVal[] vals = new JamVal[args.length];
		for(int i = 0; i < vals.length; i++){
			frame[i] = null;
		}
		
		PureList<JamVal[]> newEnv = sdEnv.cons(frame);
		for(int i = 0; i < vals.length; i++){
			vals[i] = args[i].accept(new SDEvaluator(newEnv));
		}
		((Cons<JamVal[]>)newEnv).first = vals;
		return l.body().accept(new SDEvaluator(newEnv));
	}

	public JamVal forApp(App a){
		JamVal rator = a.rator().accept(this);
		if (rator instanceof JamFun) {
			return ((JamFun)rator).accept(new FunSDEvaluator(a.args()));
		}
		throw new EvalException(rator + " appears at head of application " + a
				+ " but it is not a valid function");
	}

	public JamVal forMap(Map m){
		return new JamClosureSD(m,sdEnv);
	}

	private JamVal[] evalAll(AST[] args){
		JamVal[] vals = new JamVal[args.length];
		for(int i = 0; i < vals.length; i++){
			vals[i] = args[i].accept(this);
		}
		return vals;
	}

	class FunSDEvaluator extends FunEvaluator{
		FunSDEvaluator(AST[] asts) {
			super(asts);
		}

		public JamVal forJamClosure(JamClosure closure) {
			Map map = closure.body();
			int n = args.length;
			VariableSD[] vars = (VariableSD[]) ((MapSD)map).vars();
			if (vars.length != n) {
				throw new EvalException("closure " + closure + " applied to " + n +
						" arguments");
			}

			PureList<JamVal[]> newEnv = ((JamClosureSD)closure).sdEnv();
			newEnv = newEnv.cons(evalAll(args));
			return map.body().accept(new SDEvaluator(newEnv));
		}
	}
}


class ArityVisitor implements PrimFunVisitor<Integer>{
	public static final ArityVisitor ONLY = new ArityVisitor();
	private ArityVisitor(){}

	public Integer forFunctionPPrim() {
		return 1;
	}

	public Integer forNumberPPrim() {
		return 1;
	}

	public Integer forListPPrim() {
		return 1;
	}

	public Integer forConsPPrim() {
		return 1;
	}

	public Integer forNullPPrim() {
		return 1;
	}

	public Integer forArityPrim() {
		return 1;
	}

	public Integer forConsPrim() {
		return 2;
	}

	public Integer forFirstPrim() {
		return 1;
	}

	public Integer forRestPrim() {
		return 1;
	}

	@Override
	public Integer forRefPPrim() {
		return 1;
	}
}


