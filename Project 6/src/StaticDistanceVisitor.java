
public class StaticDistanceVisitor implements ASTVisitor<AST> {
	public static final StaticDistanceVisitor INITIAL = new StaticDistanceVisitor(new Empty<Variable[]>());
	private PureList<Variable[]> env;

	private StaticDistanceVisitor(PureList<Variable[]> env){
		this.env = env;
	}

	public AST forBoolConstant(BoolConstant b) {
		return b;
	}

	public AST forIntConstant(IntConstant i) {
		return i;
	}

	public AST forNullConstant(NullConstant n) {
		return n;
	}

	public AST forVariable(Variable v) {
		VariableSD vars = env.accept(new VariableStaticConversionVisitor(v,0));
		return vars;
	}

	@Override
	public AST forPrimFun(PrimFun f) {
		return f;
	}

	@Override
	public AST forUnOpApp(UnOpApp u) {
		return new UnOpApp(u.rator(),u.arg().accept(this));
	}

	@Override
	public AST forBinOpApp(BinOpApp b) {
		return new BinOpApp(b.rator(),b.arg1().accept(this),b.arg2().accept(this));
	}

	@Override
	public AST forApp(App a) {
		AST[] newArgs = sdAll(a.args());
		return new AppSD(a.rator().accept(this),newArgs);
	}

	@Override
	public AST forMap(Map m) {
		PureList<Variable[]> newEnv = env;
		newEnv = newEnv.cons(m.vars());
		return new MapSD(m.vars(),m.body().accept(new StaticDistanceVisitor(newEnv)));
	}

	@Override
	public AST forIf(If i) {
		return new If(i.test().accept(this),i.conseq().accept(this),i.alt().accept(this));
	}

	@Override
	public AST forLet(Let l) {
		PureList<Variable[]> newEnv = env;
		newEnv = newEnv.cons(l.vars());
		StaticDistanceVisitor visitor = new StaticDistanceVisitor(newEnv);
		AST[] exps = l.exps();
		AST[] letExps = new AST[exps.length];
		for(int i = 0; i < exps.length; i++){
			letExps[i] = exps[i].accept(this);
		}
		return new LetSD(makeDefs(l.vars(),letExps),l.body().accept(visitor));
	}

	@Override
	public AST forLetRec(LetRec l) {
		PureList<Variable[]> newEnv = env;
		newEnv = newEnv.cons(l.vars());
		StaticDistanceVisitor visitor = new StaticDistanceVisitor(newEnv);
		AST[] exps = l.exps();
		AST[] letExps = new AST[exps.length];
		for(int i = 0; i < exps.length; i++){
			letExps[i] = exps[i].accept(visitor);
		}
		return new LetRecSD(makeDefs(l.vars(),letExps),l.body().accept(visitor));
	}

	@Override
	public AST forBlock(Block b) {
		AST[] exps = new AST[b.exps.length];
		for(int i = 0; i < exps.length; i++){
			exps[i] = b.exps[i].accept(this);
		}
		return new Block(exps);
	}

	private AST[] sdAll(AST[] args){
		AST[] asts = new AST[args.length];
		for(int i = 0; i < asts.length; i++){
			asts[i] = args[i].accept(this);
		}
		return asts;
	}

	private Def[] makeDefs(Variable[] vars, AST[] exps){
		Def[] defs = new Def[vars.length];
		for(int i = 0; i < defs.length; i++){
			defs[i] = new Def(vars[i],exps[i]);
		}
		return defs;
	}

	private class VariableStaticConversionVisitor implements PureListVisitor<Variable[],VariableSD>{
		private Variable var;
		private int depth;

		VariableStaticConversionVisitor(Variable v, int depth){
			var = v;
			this.depth = depth;
		}

		@Override
		public VariableSD forEmpty(Empty<Variable[]> e) {
			return null;
		}

		@Override
		public VariableSD forCons(Cons<Variable[]> c) {
			Variable[] vars = c.first();
			for(int index = 0; index < vars.length; index++){
				if(vars[index] == var){
					return new VariableSD(depth,index);
				}
			}
			return c.rest().accept(new VariableStaticConversionVisitor(var,depth+1));
		}
	}
}

class VariableSD extends Variable{
	int sdDepth;
	int sdIndex;

	VariableSD(int depth, int index){
		super("[" + depth + "," + index + "]");
		this.sdDepth = depth;
		this.sdIndex = index;
	}

	public int sdDepth(){
		return sdDepth;
	}

	public int sdIndex(){
		return sdIndex;
	}
	
	public boolean equals(Object other){
		return (other instanceof VariableSD) && (((VariableSD)other).sdDepth() == sdDepth) && (((VariableSD)other).sdIndex() == sdIndex);
	}
}

class MapSD extends Map{
	private int numVars;
	private VariableSD[] varsSD;
	public MapSD(Variable[] mapArgs, AST mapBody){
		super(mapArgs,mapBody);
		numVars = mapArgs.length;
		varsSD = new VariableSD[mapArgs.length];
		for(int i = 0; i < varsSD.length; i++){
			varsSD[i] = new VariableSD(0,i);
		}
	}

	public int getNumVars(){
		return numVars;
	}

	public String toString(){
		return "map [*" + numVars + "*] to " + body;
	}
	
	public Variable[] vars(){
		return varsSD;
	}
}

class LetSD extends Let{
	protected int numVars;
	public LetSD(Def[] defs, AST body){
		super(defs,body);
		numVars = defs.length;
	}
	public String toString(){
		StringBuilder b = new StringBuilder();
		b.append("let [*" + numVars + "*] ");
		for(Def def : defs){
			b.append(def.rhs() + "; ");
		}
		b.append("in " + body);
		return b.toString();
	}
}

class LetRecSD extends LetRec{
	private int numVars;
	
	public LetRecSD(Def[] defs, AST body) {
		super(defs, body);
		numVars = defs.length;
	}	
	public String toString(){
		StringBuilder b = new StringBuilder();
		b.append("letrec [*" + numVars + "*] ");
		for(Def def : defs){
			b.append(def.rhs() + "; ");
		}
		b.append("in " + body);
		return b.toString();
	}
}

class AppSD extends App{
	AppSD(AST r, AST[] a) {
		super(r, a);
	}
	
	/**
	 * Matching behavior of website-provided test cases
	 */
	public String toString(){
		StringBuilder b = new StringBuilder();
		AST[] args = args();
		if(!(rator() instanceof PrimFun)){
			b.append("(" + rator() + ")(");
		}
		else{
			b.append(rator() + "(");
		}
		for(int i = 0; i < args.length; i++){
			b.append(args[i].toString());
			if(i < args.length - 1){
				b.append(", ");
			}
			else{
				b.append(")");
			}
		}
		return b.toString();
	}
}

/**
 * A Jam closure for evaluating SD-transformed programs.
 */
class JamClosureSD extends JamClosure {
	private PureList<JamVal[]> sdEnv;
	private PureList<Binding> varEnv;
	
    JamClosureSD(Map b, PureList<JamVal[]> e) {
		super(b, new Empty<Binding>());
		final int[] depth = {0};
		sdEnv = e;
		varEnv = sdEnv.accept(new PureListVisitor<JamVal[],PureList<Binding>>(){
			public PureList<Binding> forEmpty(Empty<JamVal[]> e) {
				return new Empty<Binding>();
			}


			public PureList<Binding> forCons(Cons<JamVal[]> c) {
				JamVal[] vals = c.first();
				PureList<Binding> env = new Empty<Binding>();
				for(int index = 0; index < vals.length; index++){
					env = env.cons(new ValueBinding(new VariableSD(depth[0],index),vals[index]));
				}
				depth[0] += 1;
				return env.append(c.rest().accept(this));
			}
		});
	}

    /**
     * Accessor for the environment.
     * @return environment
     */
    PureList<Binding> env() {
        return varEnv;
    }
    
    PureList<JamVal[]> sdEnv(){
    	return sdEnv;
    }

    /**
     * Visitor hook for FunVisitors.
     * @param jfv visitor to execute
     * @return visitor-specific return value
     */
    public <RtnType> RtnType accept(FunVisitor<RtnType> jfv) {
        return jfv.forJamClosure(this);
    }

    /**
     * Return a string representation of the closure.
     * @return string representation
     */
    public String toString() {
        return "JamClosureSD<" + body + ", " + sdEnv + ">";
    }
}

/**
 * Corrects the static distance of application rators;
 * [n,m] becomes [n+1,m] for n > 0, so that the non-local variables reference the same variable after
 * the application adds another frame of variables.
 * [0,m] stays [0,m], so local variable refer to the closest binding construct
 * @author Billy
 *
 */
class StaticDistanceCorrector extends ADefaultRecurseASTVisitor{
	public static final StaticDistanceCorrector ONLY = new StaticDistanceCorrector();
	private StaticDistanceCorrector(){}
	
	public AST forVariable(Variable v) {
		VariableSD vsd = (VariableSD)v;
		if(vsd.sdDepth() > 0){
			return new VariableSD(vsd.sdDepth() + 1,vsd.sdIndex());
		}
		else{
			return v;
		}
	}
	
	public AST forMap(Map m){
		AST[] asts = evalAll(m.vars());
		Variable[] vars = new Variable[asts.length];
		for(int i = 0; i < vars.length; i++){
			vars[i] = (Variable)asts[i];
		}
		return new MapSD(vars,m.body().accept(this));
	}


	public AST forDefault(AST a) {
		return a;
	}
}

class StaticDistanceFunCorrector implements FunVisitor<JamFun>{
	public static final StaticDistanceFunCorrector ONLY = new StaticDistanceFunCorrector();
	private StaticDistanceFunCorrector(){}
	public JamFun forJamClosure(JamClosure c) {
		c.body = (Map) c.body().accept(StaticDistanceCorrector.ONLY);
		return c;
	}

	@Override
	public JamFun forPrimFun(PrimFun pf) {
		return pf;
	}
}
