/**
 * A visitor used to unshadow ASTs
 *
 */
class UnshadowingVisitor implements ASTVisitor<AST> {
	private PureList<Variable> env;
	private int scope;

	public UnshadowingVisitor(int scope,PureList<Variable> env){
		this.scope = scope;
		this.env = env;
	}

	public AST forBoolConstant(BoolConstant b) {
		return b;
	}

	public AST forIntConstant(IntConstant i) {
		return i;
	}

	public AST forNullConstant(NullConstant n) {
		return n;
	}

	public AST forVariable(Variable v) {
		Variable var = env.accept(new VariableLookupVisitor(v));
		if(var != null && var.depth() > 0){
			return var;
		}
		return v;
	}

	@Override
	public AST forPrimFun(PrimFun f) {
		return f;
	}

	@Override
	public AST forUnOpApp(UnOpApp u) {
		return new UnOpApp(u.rator(),u.arg().accept(this));
	}

	@Override
	public AST forBinOpApp(BinOpApp b) {
		//if-then-else expansion of M & N and M | N
		if(b.rator() == OpAnd.ONLY){
			return new If(b.arg1().accept(this),b.arg2().accept(this),BoolConstant.FALSE);
		}
		if(b.rator() == OpOr.ONLY){
			return new If(b.arg1().accept(this),BoolConstant.TRUE,b.arg2().accept(this));
		}
		return new BinOpApp(b.rator(),b.arg1().accept(this),b.arg2().accept(this));
	}

	@Override
	public AST forApp(App a) {
		AST[] oldArgs = a.args();
		AST[] args = new AST[oldArgs.length];
		for(int i = 0; i < args.length; i++){
			args[i] = oldArgs[i].accept(this);
		}
		return new App(a.rator().accept(this),args);
	}

	@Override
	public AST forMap(Map m) {
		Variable[] vars = m.vars();
		Variable[] newVars = new Variable[vars.length];
		AST body = m.body();
		PureList<Variable> newEnv = env;
		for(int i = 0; i < vars.length; i++){
			Variable newVar = new Variable(vars[i].name(),scope+1);
			newEnv = newEnv.cons(newVar);
			newVars[i] = newVar;
		}
		UnshadowingVisitor newScope = new UnshadowingVisitor(scope+1,newEnv);
		return new Map(newVars,body.accept(newScope));
	}

	@Override
	public AST forIf(If i) {
		return new If(i.test().accept(this),i.conseq().accept(this),i.alt().accept(this));
	}

	@Override
	public AST forLet(Let l) {
		Def[] oldDefs = l.defs();
		Def[] defs = new Def[oldDefs.length];
		PureList<Variable> newEnv = env;
		for(int i = 0; i < defs.length; i++){
			Def oldDef = oldDefs[i];
			defs[i] = new Def(new Variable(oldDef.lhs().name(),scope+1),oldDef.rhs().accept(this));
			newEnv = newEnv.cons(defs[i].lhs());
		}
		UnshadowingVisitor newScope = new UnshadowingVisitor(scope+1,newEnv);
		return new Let(defs,l.body().accept(newScope));
	}

	@Override
	public AST forLetRec(LetRec l) {
		Def[] oldDefs = l.defs();
		Def[] defs = new Def[oldDefs.length];
		PureList<Variable> newEnv = env;
		for(int i = 0; i < defs.length; i++){
			Def oldDef = oldDefs[i];
			defs[i] = new Def(new Variable(oldDef.lhs().name(),scope+1),oldDef.rhs().accept(new UnshadowingVisitor(scope+1,env)));
			newEnv = newEnv.cons(defs[i].lhs());
		}
		UnshadowingVisitor newScope = new UnshadowingVisitor(scope+1,newEnv);

		//Fix lexical scope of rhs of defs
		for(int i = 0; i < defs.length; i++){
			defs[i] = new Def(defs[i].lhs(),defs[i].rhs().accept(new LexicalScopeVisitor(newEnv)));
		}
		return new LetRec(defs,l.body().accept(newScope));
	}
	
	public AST forBlock(Block b){
		AST[] exps = new AST[b.exps.length];
		for(int i = 0; i < exps.length; i++){
			exps[i] = b.exps[i].accept(this);
		}
		return new Block(exps);
	}




	private class VariableLookupVisitor extends LookupVisitor<Variable>{
		VariableLookupVisitor(Variable v){
			super(v);
		}

		public Variable forCons(Cons<Variable> c){
			Variable e = c.first();
			if (var.name().equals(e.name())) {
				return e;
			}
			return c.rest().accept(this);
		}
	}

	/**
	 * A visitor to fix the lexical scoping of the right-hand sides of defs in recursive let statements
	 *
	 */
	private class LexicalScopeVisitor implements ASTVisitor<AST>{
		private PureList<Variable> env;

		public LexicalScopeVisitor(PureList<Variable> env){
			this.env = env;
		}

		public AST forBoolConstant(BoolConstant b) {
			return b;
		}

		@Override
		public AST forIntConstant(IntConstant i) {
			return i;
		}

		@Override
		public AST forNullConstant(NullConstant n) {
			return n;
		}

		@Override
		public AST forVariable(Variable v) {
			Variable var = env.accept(new VariableLookupVisitor(v));
			if(var != null && var.depth() >= v.depth()){
				return var;
			}
			return v;
		}

		@Override
		public AST forPrimFun(PrimFun f) {
			return f;
		}

		@Override
		public AST forUnOpApp(UnOpApp u) {
			return new UnOpApp(u.rator(),u.arg().accept(this));
		}

		@Override
		public AST forBinOpApp(BinOpApp b) {
			return new BinOpApp(b.rator(),b.arg1().accept(this),b.arg2().accept(this));
		}

		@Override
		public AST forApp(App a) {
			AST[] oldArgs = a.args();
			AST[] args = new AST[oldArgs.length];
			for(int i = 0; i < args.length; i++){
				args[i] = oldArgs[i].accept(this);
			}
			return new App(a.rator().accept(this),args);
		}

		@Override
		public AST forMap(Map m) {
			Variable[] oldVars = m.vars();
			Variable[] vars = new Variable[oldVars.length];
			for(int i = 0; i < vars.length; i++){
				vars[i] = (Variable) oldVars[i].accept(this);
			}
			return new Map(vars,m.body().accept(this));
		}

		@Override
		public AST forIf(If i) {
			return new If(i.test().accept(this),i.conseq().accept(this),i.alt().accept(this));
		}

		@Override
		public AST forLet(Let l) {
			Def[] oldDefs = l.defs();
			Def[] defs = new Def[oldDefs.length];
			for(int i = 0; i < defs.length; i++){
				Def oldDef = oldDefs[i];
				defs[i] = new Def((Variable)oldDef.lhs().accept(this),oldDef.rhs().accept(this));
			}
			return new Let(defs,l.body().accept(this));
		}

		@Override
		public AST forLetRec(LetRec l) {
			Def[] oldDefs = l.defs();
			Def[] defs = new Def[oldDefs.length];
			for(int i = 0; i < defs.length; i++){
				Def oldDef = oldDefs[i];
				defs[i] = new Def((Variable)oldDef.lhs().accept(this),oldDef.rhs().accept(this));
			}
			return new LetRec(defs,l.body().accept(this));
		}

		@Override
		public AST forBlock(Block b) {
			AST[] exps = new AST[b.exps.length];
			for(int i = 0; i < exps.length; i++){
				exps[i] = b.exps[i].accept(this);
			}
			return new Block(exps);
		}
	}
}
